import matplotlib.pyplot as plt
from sklearn.datasets import fetch_olivetti_faces
import numpy as np
from face_classification import *
from statistics import mode
from sklearn.neighbors import KNeighborsClassifier as KNN



def show_features_progress():
    titles = ['Vote', 'Gradient', 'Scale', 'DCT', 'DFT', 'Histogram']
    fig, axs = plt.subplots(2, 3, figsize=(9, 8))
    for i in range(2):
        for j in range(3):
            axs[i, j].set_xlim(1*39, 9*39)
            axs[i, j].set_xticks(np.linspace(1*39, 9*39, 6).astype('int'))
            axs[i, j].set_ylim(0, 1)
            axs[i, j].set_title(titles.pop())
    return fig, axs

def make_dataset(images, fun, features_len):
    data = np.zeros((len(images), features_len))
    for i, image in enumerate(images):
        im = image * 255
        features = fun(im)
        if isinstance(features, list):
            data[i] = features
        else:
            data[i] = features.flatten()
    return data

def create_data_for_graphs():
    data_images = fetch_olivetti_faces()
    images = data_images['images']
    target = data_images['target']
    classes = np.unique(target)[-1]

    features_data = []
    functions = [histogram, get_dft, get_dct, get_scale, get_gradient]
    features_lens = [len(histogram(images[0])), len(get_dft(images[0]).flatten()), len(get_dct(images[0]).flatten()),
                        len(get_scale(images[0]).flatten()), len(get_gradient(images[0]))]


    for f, n in zip(functions, features_lens):
        features_data.append(make_dataset(images, f, n))
    print()
    return [classes, target, features_data]


def get_by_max_choice(pred_target):
    preds = np.array(pred_target)
    result_pred = []
    for i in range(len(preds[0])):
        result_pred.append(mode(preds[:, i]))
    return result_pred

def feature_progress(data):
    classes, target, features_data = data
    fig, axs = show_features_progress()
    rez = np.zeros((6, 9))
    for k in range(1, 10):
        train_data = []
        train_target = []
        test_data = []
        test_target = []

        for feature in range(5):
            train_data.append([])
            test_data.append([])
            for c in range(classes):
                if len(train_data[feature]) > 0:
                    test_data[feature] = np.concatenate((test_data[feature], features_data[feature][c*10 + k:(c+1)*10]))
                    train_data[feature] = np.concatenate((train_data[feature], features_data[feature][c*10 : c*10 + k]))
                    if feature == 0:
                        test_target = np.concatenate((test_target, target[c*10 + k:(c+1)*10]))
                        train_target = np.concatenate((train_target, target[c*10 : c*10 + k]))
                else:
                    for arr in features_data[feature][c*10 + k:(c+1)*10]:
                        test_data[feature].append(arr)
                    for arr in features_data[feature][c*10 : c*10 + k]:
                        train_data[feature].append(arr)
                    if feature == 0:
                        test_target = target[c*10 + k:(c+1)*10]
                        train_target = target[c*10 : c*10 + k]
        train_target = np.array(train_target)
        test_target = np.array(test_target)
        pred_target = []
        for feature in range(5):
            knn = KNN(n_neighbors=2)
            knn.fit(train_data[feature], train_target)
            pred_target.append(knn.predict(test_data[feature]))
        pred_target.append(get_by_max_choice(pred_target))
        acc = [np.sum(pred_target[feature] == test_target)/len(test_target) for feature in range(6)]
        rez[:, k-1] = acc
        n = 0
        for i in range(2):
            for j in range(3):
                axs[i, j].plot(np.arange(1, k+1) * classes, rez[n, :k])
                n+=1
                fig.show()
            plt.pause(1)
    fig.savefig('test.png')
# feature_progress(create_data_for_graphs())